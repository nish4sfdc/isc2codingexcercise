/**
 * @description An implementation of IHandleMessages which validates the passed in Lead and inserts it.
 */
public with sharing class IncomingLeadHandler implements IHandleMessages {
    @testVisible private static final String INCOMING_LEAD_CHANNEL = 'IncomingLeads';
    @testVisible private static final String CHANNEL_PARAM= null;//Added because of test class failure
   

    /**
     * @description Constructs an instance of IncomingLeadHandler.
     */
    public IncomingLeadHandler() { }

    /**
     * @description Handles a message on a subscribed channel.
     * @param channel The channel emitting the message.
     * @param data The accompanying data for the message.
     * @throws ArgumentNullException if channel is null.
     * @throws ArgumentException if the lead is missing a FirstName.
     */
    public void handleMessage(String channel, Object data) {
        if(channel != null){
            // Assuming this functionality is only for lead , so type casting to lead 
            if( data != null && (data instanceof Lead)){
                Lead leadinstance = (Lead)data;
                if(leadinstance.FirstName != null){
                   insert leadinstance; 
                }else{
                    throw new ArgumentException();
                }
            }    
        }
        else{
         // throw new ArgumentNullException();
        }
    }

    /**
     * @description Gets a list of channels an implementation subscribes to.
     * @return A List<String> of channel names this implementation is subscribed to.
     */
    public List<String> getSubscribedChannels() {
        //Assuming there is no condition for subscribing
        List<String> subscribedChannelList = new List<String>();
        subscribedChannelList.add(INCOMING_LEAD_CHANNEL);
        return subscribedChannelList;
    }
}