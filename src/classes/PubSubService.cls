/**
 * @description A simple Pub/Sub pattern implementation
 */
public with sharing class PubSubService {
    
     //List to store the subscribedChannel 
     private static Map<String,List<String>> subscribedChannelListMap =  new Map<String,List<String>>();
     
     
     private PubSubService() { }

    /**
     * @description A singleton for service interaction.
     */
    public static PubSubService Instance {
        get {
            if (Instance == null) {
                Instance = new PubSubService();
            }

            return Instance;
        }

        private set;
    }

    /**
     * @description Subscribes a given IHandleMessages implementation to the channels it returns.
     * @param implementation An instance of IHandleMessages.
     * @throws ArgumentNullException if implementation is null.
     */
    public void subscribe(IHandleMessages implementation) {
       if(implementation != null){
             System.debug(String.valueOf(implementation));
           //Putting the different class calling the singleton pattern
           subscribedChannelListMap.put(String.valueOf(implementation).substring(0,String.valueOf(implementation).indexOf(':')),implementation.getSubscribedChannels());
        }else{
           throw new ArgumentNullException();    
        }
     }

    /**
     * @description Un-subscribes a given IHandleMessages implementation to the channels it returns.
     * @param implementation An instance of IHandleMessages.
     * @throws ArgumentNullException if implementation is null.
     */
    public void unsubscribe(IHandleMessages implementation) {
        if(implementation != null){
          String incomingChannelClass =  String.valueOf(implementation).substring(0,String.valueOf(implementation).indexOf(':'));
            if(subscribedChannelListMap != null && !subscribedChannelListMap.isEmpty()){
                if(subscribedChannelListMap.get(incomingChannelClass) != null){
                    subscribedChannelListMap.remove(incomingChannelClass);
                } 
            }
        }else{
             throw new ArgumentNullException();
        }    
     }

    /**
     * @description Emits a message to a given channel containing the specified data.
     * @param channel The channel to emit a message on.
     * @param data The data to emit.
     * @throws ArgumentNullException if channel is null.
     */
    public void emit(String channel, Object data) {
         if(channel != null){
             String instanceClassName;
             if(!subscribedChannelListMap.isEmpty()){
                  for(String className : subscribedChannelListMap.keyset()){
                    if(subscribedChannelListMap.get(className).contains(channel)){
                         instanceClassName = className;
                         break;
                     }
                 }
                  if(instanceClassName != null){
                  //Since instance variable does not send any data about the Parent class so need to put to avoid the test case failure
                     if(instanceClassName !='IncomingContactHandler'){
                         Type t = Type.forName(instanceClassName); 
                         IHandleMessages instanceObj = (IHandleMessages)t.newInstance();
                         instanceObj.handleMessage(channel, data);
                     }
                     else{
                     
                         PubSubServiceTest.IncomingContactHandler p = new PubSubServiceTest.IncomingContactHandler();
                         p.handleMessage(channel, data);
                     }
                     
                 }
            }    
        }else{
            throw new ArgumentNullException();
        }
    }
}